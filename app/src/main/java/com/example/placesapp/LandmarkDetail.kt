package com.example.placesapp

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


class LandmarkDetail : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var landmark: Landmark
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landmark_detail)

        landmark = intent?.getParcelableExtra(LANDMARK_DETAIL)?: Landmark(name = "Chincoteague",
            category = "Rivers", city = "Chincoteague", state = "Virginia", id=1011,
            park="Chincoteague National Wildlife Refuge",
            coordinates = Coordinate(-75.383212, 37.91531),
            imageName = "chincoteague"
        )

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map_fragment) as SupportMapFragment

        mapFragment.getMapAsync(this)

        val name = findViewById<TextView>(R.id.landmark_detail_name)
        val park = findViewById<TextView>(R.id.landmark_detail_park)
        val state = findViewById<TextView>(R.id.landmark_detail_state)

        name.text = landmark.name
        park.text = landmark.park
        state.text = landmark.state

        val image = findViewById<ImageView>(R.id.landmark_detail_image)
        val imageResource: Int = getResources()
            .getIdentifier(landmark.imageName, "drawable", packageName)
        image.setBackgroundResource(imageResource)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        val latlong = LatLng(landmark.coordinates.latitude, landmark.coordinates.longitude)
        val point = CameraUpdateFactory.newLatLngZoom(latlong, 7f)
        googleMap.moveCamera(point)
        googleMap.animateCamera(point)
        googleMap.addMarker(
            MarkerOptions()
                .position(latlong)
                .title(landmark.name)
        )
    }
    companion object{
        const val LANDMARK_DETAIL="LANDMARK_DETAIL"
    }
}