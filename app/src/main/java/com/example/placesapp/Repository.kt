package com.example.placesapp

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonParser
import java.lang.Exception

class Repository {
    companion object{
        const val TAG_REPOSITORY = "Repository"
    }
    fun getLandmarkDataSet(context: Context):List<Landmark>{
        val landmarks = mutableListOf<Landmark>()
        try{
            val jsonString = getJsonFromAssets(context, "landmarkData.json")

            val jsonArray = JsonParser.parseString(jsonString).asJsonArray
            jsonArray.forEach {
                val landmark = Gson().fromJson(it, Landmark::class.java)
                landmarks.add(landmark)
            }
        }
        catch (e:Exception){
            Log.d(TAG_REPOSITORY, "An exception occurred while obtaining the landmark data from landmarkData.json")
            e.message?.let { Log.d(TAG_REPOSITORY, it) }
            Log.d(TAG_REPOSITORY, "Error: ${e}")
        }
        return landmarks
    }
}