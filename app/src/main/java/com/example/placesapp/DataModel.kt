package com.example.placesapp

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Landmark(val name: String,
                    val category: String,
                    val city: String,
                    val state: String,
                    val id:Int,
                    val park:String,
                    val coordinates: Coordinate,
                    val imageName:String):Parcelable
@Parcelize
data class Coordinate(val longitude: Double, val latitude: Double):Parcelable