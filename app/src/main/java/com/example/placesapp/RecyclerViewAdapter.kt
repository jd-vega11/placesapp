package com.example.placesapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewAdapter(private val myDataset: List<Landmark>,
private val callback: (landmark:Landmark) -> Unit) :
    RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>() {
    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): RecyclerViewAdapter.MyViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list, parent, false)

        return MyViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        val landmark = myDataset[position]
        val image = holder.view.findViewById<ImageView>(R.id.landmark_list_image)
        val context: Context = image.context
        val imageResource: Int = context.getResources()
            .getIdentifier(landmark.imageName, "drawable", context.packageName)
        image.setBackgroundResource(imageResource)

        val name = holder.view.findViewById<TextView>(R.id.landmark_list_name)
        name.text = landmark.name

        holder.view.setOnClickListener{
            callback(landmark)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
