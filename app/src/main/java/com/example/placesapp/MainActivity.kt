package com.example.placesapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var repository: Repository
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        repository = Repository()
        viewManager = LinearLayoutManager(this)
        val dataSet = repository.getLandmarkDataSet(this)
        Log.d("Revision", "data ${dataSet.forEach { it.name }}")
        viewAdapter = RecyclerViewAdapter(dataSet, ::goToLandmarkDetail)
        recyclerView = findViewById<RecyclerView>(R.id.recycler_view_places).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter

        }
    }

    fun goToLandmarkDetail(landmark: Landmark){
        val intent = Intent(this, LandmarkDetail::class.java)
        intent.putExtra(LandmarkDetail.LANDMARK_DETAIL, landmark as Parcelable)
        startActivity(intent)
    }
}